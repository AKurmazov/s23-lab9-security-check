

# OWASP CSS

Resource: https://www.drom.ru/

### №1: Forgot Password 

| Step                                                                    | Result                                               |
|:----------------------------------------------------------------------- | ---------------------------------------------------- |
| Open https://www.drom.ru/                                               | **OK**, opened                                           |
| Click on the **Вход и регистрация** reference                           | **OK**, the sign-in page is opened                       |
| Click on the **Напомнить пароль** reference                             | **OK**, the restore password page is opened              |
| Write a registered email and click on the **Напомнить пароль** button   | **OK**, the email is sent                                |
| Check out the received email                                            | **Not OK**, the plain password is sent                   |
| Return to the **forgot password** page and submit an unregistered email | **Not OK**, a pop-up told me that account does not exist |
| Access time taken on several different submissions                      | **OK**, time taken is uniform                            |


**Verdict:**
1. Account enumeration attack is possible
2. The new password is sent in the email, which is not secure

### №2: File Upload



| Step                                                       | Result                                                                 |
| ---------------------------------------------------------- | ---------------------------------------------------------------------- |
| Open https://www.drom.ru/                                  | **OK**, opened                                                             |
| Being unauthenticated, try to find a file upload form      | **OK**, could not find                                                     |
| Authenticate and click on the **Подать объявление** button | **OK**, the page is opened                                                 |
| Click on the **Загрузить СТС** reference                   | **OK**, the file extensions are validated, and only image ones are allowed |
| Click again and try to upload a large image                | **OK**, size validation has restricted the upload                          |
| Check the name of the uploaded file                        | **OK**, the name is generated and does not correspond to the initial one   |

**Verdict:**
1. Every thing is fine

